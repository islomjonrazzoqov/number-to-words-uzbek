var input=document.querySelector("#inputNumber");
var p=document.querySelector('#wordNumber');
input.addEventListener("input",function(){
    var shkala=['','ming','million','milliard','trillion','kvadrillion','kvintillon','sextillion','septillon','oktillion','nonalion','decalion','nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion']
    var txt=input.value;
    var start=txt.length;
    var a=[];
    while(start>0){
        var end=start;
        a.push(txt.slice(start=Math.max(0,start-3),end));
    }
    a=a.reverse();
    var word="";
    if(txt.length<=75){
        while(a.length>0){
            console.log(txt.length);
            word+=numberToWords(a[0]);
            if(a[0]!=="000")
                word+="  "+shkala[a.length-1]+"  ";
            a.splice(0,1);
        }
        p.innerHTML=word;
    }
    else p.innerHTML="Error!!!"
    
});
function numberToWords(str){
    var son=parseInt(str);
    var yuzlik=parseInt(son/100);
    var unlik=parseInt((son/10)%10);
    var birlik=parseInt(son%10);
    var temp="";
    switch(yuzlik){
        case 0:temp+="";break;
        case 1:temp+="bir yuz ";break;
        case 2:temp+="ikki yuz ";break;
        case 3:temp+="uch yuz ";break;
        case 4:temp+="to`rt yuz ";break;
        case 5:temp+="besh yuz ";break;
        case 6:temp+="olti yuz ";break;
        case 7:temp+="yetti yuz ";break;
        case 8:temp+="sakkiz yuz ";break;
        case 9:temp+="to`qqiz yuz ";break;
    }
    switch(unlik){
        case 0:temp+="";break;
        case 1:temp+="o`n ";break;
        case 2:temp+="yigirma ";break;
        case 3:temp+="o`ttiz ";break;
        case 4:temp+="qirq ";break;
        case 5:temp+="ellik ";break;
        case 6:temp+="oltmish ";break;
        case 7:temp+="yetmish ";break;
        case 8:temp+="sakson ";break;
        case 9:temp+="to`qson ";break;
    }
    switch(birlik){
        case 0:temp+="";break;
        case 1:temp+="bir";break;
        case 2:temp+="ikki";break;
        case 3:temp+="uch";break;
        case 4:temp+="to`rt";break;
        case 5:temp+="besh";break;
        case 6:temp+="olti";break;
        case 7:temp+="yetti";break;
        case 8:temp+="sakkiz";break;
        case 9:temp+="to`qqiz";break;
    }
    return temp;
}